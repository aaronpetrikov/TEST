from datetime import date
import datetime

startDateString = "2022-10-01"
endDateString = "2023-12-01"

startDateArray = startDateString.split("-")
startDate = date(int(startDateArray[0]),int(startDateArray[1]),int(startDateArray[2]))
endDateArray = endDateString.split("-")
endDate = date(int(endDateArray[0]),int(endDateArray[1]),int(endDateArray[2]))
time_change = datetime.timedelta(days=30)

t0 = startDate
while(t0 < endDate):
    initIndex = 0
    t1 = t0 + time_change
    url = "https://services.nvd.nist.gov/rest/json/cves/2.0/?pubStartDate="+str(t0)+"T00:00:00.000&pubEndDate="+str(t1)+"T00:00:00.000"+"&resultsPerPage=2000&startIndex=0"
    print("FETCHING : "+str(t0)+"-"+str(t1)+"_InitIndex="+str(initIndex))
    x = requests.get(url)
    print(x.status_code + "\n")
    jsonObj = json.loads(x.text)
    with open("./Fetching_RESULT/FETCH_"+str(t0)+"-"+str(t1)+"_InitIndex="+str(initIndex)+".json", "w") as outfile:
        json.dump(jsonObj,outfile,indent=4)
    while(jsonObj['totalResults']-jsonObj['startIndex']>2000):
        initIndex += 2000
        url = "https://services.nvd.nist.gov/rest/json/cves/2.0/?pubStartDate="+str(t0)+"T00:00:00.000&pubEndDate="+str(t1)+"T00:00:00.000"+"&resultsPerPage=2000&startIndex="+str(initIndex)
        print("FETCHING : "+str(t0)+"-"+str(t1)+"_InitIndex="+str(initIndex))
        x = requests.get(url)
        print(x.status_code + "\n")
        jsonObj = json.loads(x.text)
        with open("./Fetching_RESULT/FETCH_"+str(t0)+"-"+str(t1)+"_InitIndex="+str(initIndex)+".json", "w") as outfile:
            json.dump(jsonObj,outfile,indent=4)
    t0 = t1